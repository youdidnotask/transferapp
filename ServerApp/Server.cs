﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ServerApp {
    class Server {
        private static long index = 0;
        IPAddress localAddress = IPAddress.Parse("127.0.0.1");
        private Int32 port = 4000;
        private string directory = @"D:\files\";
        private TcpListener listener;
        private TcpClient client;
        public Byte[] codeEOF = new Byte[4] { 1, 1, 1, 1 };
        public Byte[] codeERROR = new Byte[4] { 1, 1, 1, 0 };
        bool isEOF, isERROR;

        public void Run() {
            NetworkStream netStream = null;
            FileStream fileStream = null;
            Byte[] part;
            Int32 kb = 1024;
            Int32 num;
            Int32 length;

            CreateDirectory();

            try {
                listener = new TcpListener(localAddress, port);
                listener.Start();
                Console.WriteLine("Server is running...");

                while(true) {
                    try {
                        client = listener.AcceptTcpClient();
                        netStream = client.GetStream();
                        Console.WriteLine("New connection");

                        while(true) {
                            while(!netStream.DataAvailable) { }

                            fileStream = new FileStream(String.Format(directory + "file{0}", index), FileMode.Create);
                            part = new byte[kb];
                            num = 0;
                            isEOF = false;
                            isERROR = false;

                            do {
                                netStream.Flush();
                                num = netStream.Read(part, 0, part.Length);
                                if (part.Length < kb) Console.WriteLine("YEP!");
                                if (num != 0) {
                                    fileStream.Write(part, 0, part.Length);
                                    fileStream.Flush();
                                }
                            } while (!Verify(num, part));

                            fileStream.Close();
                            if(isEOF) {
                                Console.WriteLine("File saved");
                            }
                            else if(isERROR) {
                                File.Delete(String.Format(directory + "file{0}", index));
                                Console.WriteLine("Error");
                            }
                            index++;
                        }

                    } catch(Exception ex) {
                        Console.WriteLine(ex.StackTrace);
                    } finally {
                        if (fileStream != null) fileStream.Close();
                        if (netStream != null) netStream.Close();
                        if (client != null) client.Close();
                    }
                }

            } catch (ArgumentNullException ex) {
                Console.WriteLine(ex.StackTrace);
            } catch (ArgumentOutOfRangeException ex) {
                Console.WriteLine(ex.StackTrace);
            } catch (OutOfMemoryException ex) {
                Console.WriteLine(ex.StackTrace);
            } finally {
                if (listener != null) listener.Stop();
            }
        }

        private void CreateDirectory() {
            if(!Directory.Exists(directory)) {
                Directory.CreateDirectory(directory);
            }
        }

        private bool Verify(Int32 num, Byte[] part) {
            if (num != 4) return false;
            return (VerifyEOF(part) || VerifyERROR(part)); 
        }

        private bool VerifyEOF(Byte[] part) {
            for(int i=0; i<codeEOF.Length; ++i) {
                if (part[i] != codeEOF[i]) return false;
            }
            isEOF = true;
            return true;
        }

        private bool VerifyERROR(Byte[] part) {
            for (int i = 0; i < codeERROR.Length; ++i) {
                if (part[i] != codeERROR[i]) return false;
            }
            isERROR = true;
            return true;
        }

    }
}
