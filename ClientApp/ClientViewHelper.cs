﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApp {
    public partial class TransferForm : Form {
        private String requiredInfo = "required";
        private String invalidValueInfo = "invalid value";
        private String connErrorInfo = "connection error";
        private String invalidFileInfo = "invalid file";
        private String fileSentInfo = "file sent succesfully";
        private String fileDidNotSendInfo = "file sending error";
        private String fileSendingTerminatedInfo = "Sending terminated";
        private String noFileSelectedInfo = "no file selected";

        private void init() {
            connStatusInfoLabel.Text = ConnectionStatus.DISCONNECTED.ToString();
            transferStatusInfoLabel.Text = TransferStatus.NO_ACTION.ToString();

            ipAddressTextBox.Text = host;
            ipAddressErrorLabel.Text = invalidValueInfo;
            ipAddressErrorLabel.Visible = false;

            portTextBox.Text = port.ToString();
            portErrorLabel.Text = invalidValueInfo;
            portErrorLabel.Visible = false;

            connErrorLabel.Text = connErrorInfo;
            connErrorLabel.Visible = false;

            SetDisconnectedState();
        }

        private void SetConnectedState() {
            connStatusInfoLabel.Text = ConnectionStatus.CONNECTED.ToString();
            fileNameLabel.Text = noFileSelectedInfo;
            connectButton.Enabled = false;
            disconnectButton.Enabled = true;
            selectFileButton.Enabled = true;
            sendButton.Enabled = false;
            stopButton.Enabled = false;
        }

        private void SetDisconnectedState() {
            connStatusInfoLabel.Text = ConnectionStatus.DISCONNECTED.ToString();
            transferStatusInfoLabel.Text = TransferStatus.NO_ACTION.ToString();
            transferProgressBar.Value = 0;
            fileNameLabel.Text = "";
            connectButton.Enabled = true;
            disconnectButton.Enabled = false;
            selectFileButton.Enabled = false;
            sendButton.Enabled = false;
            stopButton.Enabled = false;
        }

        private void SetFileLoadedState() {
            fileNameLabel.Text = openFileDialog1.SafeFileName;
            sendButton.Enabled = true;
            stopButton.Enabled = true;
        }

        private void SetFileSendingState() {
            transferStatusInfoLabel.Text = TransferStatus.START_SENDING.ToString();
        }

        private void SetFileSentStatus() {
            MessageBox.Show(fileSentInfo);
        }

        private void SetFileSendingErrorStatus() {
            MessageBox.Show(fileDidNotSendInfo);
        }

        private void SetFileSendingTerminatedStatus() {
            MessageBox.Show(fileSendingTerminatedInfo);
        }

        private void ClearErrors() {
            ipAddressErrorLabel.Visible = false;
            portErrorLabel.Visible = false;
            connErrorLabel.Visible = false;
        }

        private void SetConnectionError() {
            connErrorLabel.Visible = true;
        }

        private void SetFileError() {
            fileNameLabel.Text = invalidFileInfo;
        }

        private Boolean CheckIpAddressLabel() {
            if (String.IsNullOrEmpty(ipAddressTextBox.Text)) {
                ipAddressErrorLabel.Visible = true;
                return false;
            }
            host = ipAddressTextBox.Text;
            return true;
        }

        private Boolean CheckPortLabel() {
            if (String.IsNullOrEmpty(portTextBox.Text)) {
                portErrorLabel.Visible = true;
                return false;
            }
            try {
                port = Int32.Parse(portTextBox.Text);
            }
            catch(Exception) {
                portErrorLabel.Visible = true;
                return false;
            }
            return true;
        }

        private void RefreshTransferState() {
            transferProgressBar.Value = transferState;
        }
    }
}
