﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientApp {
    enum ConnectionStatus {
        CONNECTED,
        DISCONNECTED
    }

    enum TransferStatus {
        NO_ACTION,
        START_SENDING,
        SENT,
        STOPPED
    }

    public class SendingTerminatedException : Exception {}
}
