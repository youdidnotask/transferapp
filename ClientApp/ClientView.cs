﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientApp {
    public partial class TransferForm : Form {
        private String host = "127.0.0.1";
        private Int32 port = 4000;
        private TcpClient client;
        NetworkStream netStream;
        private string filePath;
        private volatile bool terminated = false;
        private volatile int transferState;
        public Byte[] codeEOF = new Byte[4] { 1, 1, 1, 1 };
        public Byte[] codeERROR = new Byte[4] { 1, 1, 1, 0 };

        public TransferForm() {
            InitializeComponent();
            init();
        }

        private Boolean Connect() {
            try {
                client = new TcpClient(host, port);
                netStream = client.GetStream();
                return true;
            }
            catch(ArgumentNullException) {
                return false;
            }
            catch(ArgumentOutOfRangeException) {
                return false;
            }
            catch(SocketException) {
                return false;
            }
            catch(Exception) {
                return false;
            }
        }

        private void Disconnect() {
            if (netStream != null) {
                netStream.Close();
            }
            if (client != null) {
                client.Close();
            }
        }

        private bool VerifyConnection() {
            return (client != null && client.Connected);
        }

        private void CalculateTransferState(long length, long sent) {
            transferState = (int)((sent * 100L) / length);
        }

        private bool LoadFilePath() {
            filePath = openFileDialog1.FileName;
            return File.Exists(filePath);
        }

        private void SendFile (String path) {
            Int32 kb = 1024;
            Byte[] part;
            long length, sent;
            Int32 num;

            Monitor.Enter(client);

            terminated = false;
            transferState = 0;
            FileStream fileStream = null;
            part = new Byte[kb];
            sent = 0;
            num = 0;

            try {
                fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
                length = fileStream.Length;

                do {
                    if (terminated) {
                        netStream.Write(codeERROR, 0, codeERROR.Length);
                        netStream.Flush();
                        throw new SendingTerminatedException();
                    }
                    fileStream.Flush();
                    num = fileStream.Read(part, 0, kb);
                    netStream.Write(part, 0, num);
                    netStream.Flush();

                    sent += num;
                    CalculateTransferState(length, sent);
                } while (num != 0);

                netStream.Write(codeEOF, 0, codeEOF.Length);
                netStream.Flush();

                SetFileSentStatus();
            }
            catch (SendingTerminatedException) {
                SetFileSendingTerminatedStatus();
            }
            catch (Exception) {
                SetFileSendingErrorStatus();
            }
            finally {
                if(fileStream != null) {
                    fileStream.Close();
                }
                Monitor.Exit(client);
            }
        }

        public void connectButton_Click(object sender, EventArgs e) {
            ClearErrors();
            if(CheckIpAddressLabel() & CheckPortLabel()) {
                if(Connect()) {
                    SetConnectedState();
                }
                else {
                    SetConnectionError();
                }
            }
        }

        private void disconnectButton_Click(object sender, EventArgs e) {
            Disconnect();
            SetDisconnectedState();
        }

        private void exitButton_Click(object sender, EventArgs e) {
            Disconnect();
            Application.Exit();
        }

        private void selectFileButton_Click(object sender, EventArgs e) {
            if (openFileDialog1.ShowDialog() == DialogResult.OK) {
                if (LoadFilePath()) {
                    SetFileLoadedState();
                }
                else {
                    SetFileError();
                }
            }
        }

        private void sendButton_Click(object sender, EventArgs e) {
            if (VerifyConnection()) {
                SetFileSendingState();
                (new Thread(() => {
                    SendFile(filePath);
                })).Start();
            }
            else {
                SetDisconnectedState();
            }
        }

        private void stopButton_Click(object sender, EventArgs e) {
            terminated = true;
        }

        private void timer1_Tick(object sender, EventArgs e) {
            RefreshTransferState();
        }
    }
}
