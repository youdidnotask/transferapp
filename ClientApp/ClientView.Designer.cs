﻿namespace ClientApp {
    partial class TransferForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.connectButton = new System.Windows.Forms.Button();
            this.connStatusTitleLabel = new System.Windows.Forms.Label();
            this.connStatusInfoLabel = new System.Windows.Forms.Label();
            this.ipAddressLabel = new System.Windows.Forms.Label();
            this.portLabel = new System.Windows.Forms.Label();
            this.ipAddressTextBox = new System.Windows.Forms.TextBox();
            this.portTextBox = new System.Windows.Forms.TextBox();
            this.disconnectButton = new System.Windows.Forms.Button();
            this.selectFileLabel = new System.Windows.Forms.Label();
            this.selectFileButton = new System.Windows.Forms.Button();
            this.fileNameLabel = new System.Windows.Forms.Label();
            this.sendButton = new System.Windows.Forms.Button();
            this.stopButton = new System.Windows.Forms.Button();
            this.transferStatusTitleLabel = new System.Windows.Forms.Label();
            this.transferStatusInfoLabel = new System.Windows.Forms.Label();
            this.exitButton = new System.Windows.Forms.Button();
            this.ipAddressErrorLabel = new System.Windows.Forms.Label();
            this.portErrorLabel = new System.Windows.Forms.Label();
            this.connErrorLabel = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.transferProgressBar = new System.Windows.Forms.ProgressBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(43, 160);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(75, 23);
            this.connectButton.TabIndex = 0;
            this.connectButton.Text = "connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // connStatusTitleLabel
            // 
            this.connStatusTitleLabel.AutoSize = true;
            this.connStatusTitleLabel.Location = new System.Drawing.Point(40, 40);
            this.connStatusTitleLabel.Name = "connStatusTitleLabel";
            this.connStatusTitleLabel.Size = new System.Drawing.Size(40, 13);
            this.connStatusTitleLabel.TabIndex = 1;
            this.connStatusTitleLabel.Text = "Status:";
            // 
            // connStatusInfoLabel
            // 
            this.connStatusInfoLabel.AutoSize = true;
            this.connStatusInfoLabel.Location = new System.Drawing.Point(120, 40);
            this.connStatusInfoLabel.Name = "connStatusInfoLabel";
            this.connStatusInfoLabel.Size = new System.Drawing.Size(73, 13);
            this.connStatusInfoLabel.TabIndex = 2;
            this.connStatusInfoLabel.Text = "Disconnected";
            // 
            // ipAddressLabel
            // 
            this.ipAddressLabel.AutoSize = true;
            this.ipAddressLabel.Location = new System.Drawing.Point(40, 80);
            this.ipAddressLabel.Name = "ipAddressLabel";
            this.ipAddressLabel.Size = new System.Drawing.Size(54, 13);
            this.ipAddressLabel.TabIndex = 3;
            this.ipAddressLabel.Text = "IP adress:";
            // 
            // portLabel
            // 
            this.portLabel.AutoSize = true;
            this.portLabel.Location = new System.Drawing.Point(40, 120);
            this.portLabel.Name = "portLabel";
            this.portLabel.Size = new System.Drawing.Size(29, 13);
            this.portLabel.TabIndex = 4;
            this.portLabel.Text = "Port:";
            // 
            // ipAddressTextBox
            // 
            this.ipAddressTextBox.Location = new System.Drawing.Point(123, 78);
            this.ipAddressTextBox.Name = "ipAddressTextBox";
            this.ipAddressTextBox.Size = new System.Drawing.Size(120, 20);
            this.ipAddressTextBox.TabIndex = 5;
            // 
            // portTextBox
            // 
            this.portTextBox.Location = new System.Drawing.Point(123, 117);
            this.portTextBox.Name = "portTextBox";
            this.portTextBox.Size = new System.Drawing.Size(120, 20);
            this.portTextBox.TabIndex = 6;
            // 
            // disconnectButton
            // 
            this.disconnectButton.Location = new System.Drawing.Point(168, 160);
            this.disconnectButton.Name = "disconnectButton";
            this.disconnectButton.Size = new System.Drawing.Size(75, 23);
            this.disconnectButton.TabIndex = 7;
            this.disconnectButton.Text = "disconnect";
            this.disconnectButton.UseVisualStyleBackColor = true;
            this.disconnectButton.Click += new System.EventHandler(this.disconnectButton_Click);
            // 
            // selectFileLabel
            // 
            this.selectFileLabel.AutoSize = true;
            this.selectFileLabel.Location = new System.Drawing.Point(300, 40);
            this.selectFileLabel.Name = "selectFileLabel";
            this.selectFileLabel.Size = new System.Drawing.Size(56, 13);
            this.selectFileLabel.TabIndex = 8;
            this.selectFileLabel.Text = "Select file:";
            // 
            // selectFileButton
            // 
            this.selectFileButton.Location = new System.Drawing.Point(303, 70);
            this.selectFileButton.Name = "selectFileButton";
            this.selectFileButton.Size = new System.Drawing.Size(75, 23);
            this.selectFileButton.TabIndex = 9;
            this.selectFileButton.Text = "select";
            this.selectFileButton.UseVisualStyleBackColor = true;
            this.selectFileButton.Click += new System.EventHandler(this.selectFileButton_Click);
            // 
            // fileNameLabel
            // 
            this.fileNameLabel.AutoSize = true;
            this.fileNameLabel.Location = new System.Drawing.Point(400, 75);
            this.fileNameLabel.Name = "fileNameLabel";
            this.fileNameLabel.Size = new System.Drawing.Size(78, 13);
            this.fileNameLabel.TabIndex = 10;
            this.fileNameLabel.Text = "no file selected";
            // 
            // sendButton
            // 
            this.sendButton.Location = new System.Drawing.Point(303, 110);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(75, 23);
            this.sendButton.TabIndex = 11;
            this.sendButton.Text = "send";
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler(this.sendButton_Click);
            // 
            // stopButton
            // 
            this.stopButton.Location = new System.Drawing.Point(403, 110);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(75, 23);
            this.stopButton.TabIndex = 12;
            this.stopButton.Text = "stop";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // transferStatusTitleLabel
            // 
            this.transferStatusTitleLabel.AutoSize = true;
            this.transferStatusTitleLabel.Location = new System.Drawing.Point(300, 150);
            this.transferStatusTitleLabel.Name = "transferStatusTitleLabel";
            this.transferStatusTitleLabel.Size = new System.Drawing.Size(40, 13);
            this.transferStatusTitleLabel.TabIndex = 13;
            this.transferStatusTitleLabel.Text = "Status:";
            // 
            // transferStatusInfoLabel
            // 
            this.transferStatusInfoLabel.AutoSize = true;
            this.transferStatusInfoLabel.Location = new System.Drawing.Point(346, 150);
            this.transferStatusInfoLabel.Name = "transferStatusInfoLabel";
            this.transferStatusInfoLabel.Size = new System.Drawing.Size(51, 13);
            this.transferStatusInfoLabel.TabIndex = 14;
            this.transferStatusInfoLabel.Text = "no action";
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(479, 216);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(75, 23);
            this.exitButton.TabIndex = 15;
            this.exitButton.Text = "exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // ipAddressErrorLabel
            // 
            this.ipAddressErrorLabel.AutoSize = true;
            this.ipAddressErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.ipAddressErrorLabel.Location = new System.Drawing.Point(123, 98);
            this.ipAddressErrorLabel.Name = "ipAddressErrorLabel";
            this.ipAddressErrorLabel.Size = new System.Drawing.Size(31, 13);
            this.ipAddressErrorLabel.TabIndex = 16;
            this.ipAddressErrorLabel.Text = "none";
            // 
            // portErrorLabel
            // 
            this.portErrorLabel.AutoSize = true;
            this.portErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.portErrorLabel.Location = new System.Drawing.Point(123, 135);
            this.portErrorLabel.Name = "portErrorLabel";
            this.portErrorLabel.Size = new System.Drawing.Size(31, 13);
            this.portErrorLabel.TabIndex = 17;
            this.portErrorLabel.Text = "none";
            // 
            // connErrorLabel
            // 
            this.connErrorLabel.AutoSize = true;
            this.connErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.connErrorLabel.Location = new System.Drawing.Point(43, 190);
            this.connErrorLabel.Name = "connErrorLabel";
            this.connErrorLabel.Size = new System.Drawing.Size(31, 13);
            this.connErrorLabel.TabIndex = 18;
            this.connErrorLabel.Text = "none";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // transferProgressBar
            // 
            this.transferProgressBar.Location = new System.Drawing.Point(303, 177);
            this.transferProgressBar.Name = "transferProgressBar";
            this.transferProgressBar.Size = new System.Drawing.Size(175, 13);
            this.transferProgressBar.Step = 1;
            this.transferProgressBar.TabIndex = 19;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // TransferForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 262);
            this.Controls.Add(this.transferProgressBar);
            this.Controls.Add(this.connErrorLabel);
            this.Controls.Add(this.portErrorLabel);
            this.Controls.Add(this.ipAddressErrorLabel);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.transferStatusInfoLabel);
            this.Controls.Add(this.transferStatusTitleLabel);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.sendButton);
            this.Controls.Add(this.fileNameLabel);
            this.Controls.Add(this.selectFileButton);
            this.Controls.Add(this.selectFileLabel);
            this.Controls.Add(this.disconnectButton);
            this.Controls.Add(this.portTextBox);
            this.Controls.Add(this.ipAddressTextBox);
            this.Controls.Add(this.portLabel);
            this.Controls.Add(this.ipAddressLabel);
            this.Controls.Add(this.connStatusInfoLabel);
            this.Controls.Add(this.connStatusTitleLabel);
            this.Controls.Add(this.connectButton);
            this.Name = "TransferForm";
            this.Text = "File Transfer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.Label connStatusTitleLabel;
        private System.Windows.Forms.Label connStatusInfoLabel;
        private System.Windows.Forms.Label ipAddressLabel;
        private System.Windows.Forms.Label portLabel;
        private System.Windows.Forms.TextBox ipAddressTextBox;
        private System.Windows.Forms.TextBox portTextBox;
        private System.Windows.Forms.Button disconnectButton;
        private System.Windows.Forms.Label selectFileLabel;
        private System.Windows.Forms.Button selectFileButton;
        private System.Windows.Forms.Label fileNameLabel;
        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.Label transferStatusTitleLabel;
        private System.Windows.Forms.Label transferStatusInfoLabel;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Label ipAddressErrorLabel;
        private System.Windows.Forms.Label portErrorLabel;
        private System.Windows.Forms.Label connErrorLabel;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ProgressBar transferProgressBar;
        private System.Windows.Forms.Timer timer1;
    }
}

